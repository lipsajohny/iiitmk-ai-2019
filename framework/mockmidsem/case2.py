import sys
from mock_midsem import submission

inputs = [i.split() for i in ["+ 1.1 2.2"]]
outputs = ["3.3"]
for args, exp in zip(inputs, outputs):
    out = submission(*args)
    if out != exp:
        print(f"Input   : {args}")
        print(f"Expected: {exp}")
        print(f"Got     : {out}")
        sys.exit(1)
print("OK! Case2 passed")
